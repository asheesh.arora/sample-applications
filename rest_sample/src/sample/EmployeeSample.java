package sample;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ABC")
public class EmployeeSample
{
    @GET
    @Path("/Emp")
    @Produces(MediaType.APPLICATION_JSON)
    public Employee getEmployee(String name)
    {
        Employee e = new Employee();
        e.name ="Bhushan";
        e.mobile = "9850883037";
        return e;
    }
}
