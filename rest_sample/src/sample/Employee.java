package sample;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee
{
    public String name;
    public String mobile;
}
